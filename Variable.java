package project;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
//import scala.tools.scalap.Classfile;

import java.io.*;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Vector;

public class Variable {
    /*
     * Parameters:
     * oriTable
     * tfTableName
     * groupSize
     * dsUpperBound
     * dsLowerBound
     * revisionRange
     * colTransformInfos: colName, expression, newColName
     * oriPKColNames
     * oriAKColNames
     * oriAbstOptColNames
     * oriAbstColNames
     * colAbstInfos
     * oriSyncColNames
     * projectName
     */
    
    
    public static Logger logger = Logger.getLogger(Operation.class.getName());

    public static String url = new String();
    public static String username = new String();
    public static String password = new String();

    public static String oriTable = new String();
    public static String tfTableName = new String();
    public static int groupSize = 3;
    public static int roundNum = 3;
    public static double dsUpperBound = 1.0;
    public static double dsLowerBound = 0.0;
    public static double revisionRange = 0.05;
    public static Vector<Vector<String>> colTransformInfos = new Vector<>();
    public static Vector<String> oriPKColNames = new Vector<>();
    public static Vector<String> oriAKColNames = new Vector<>();
    public static Vector<Double> oriAKColDomainRanges = new Vector<>();
    public static Vector<String> oriAbstOptColNames = new Vector<>();
    public static Vector<Integer> oriAbstOptColNamesOrder = new Vector<>();
    public static Vector<String> oriAbstColNames = new Vector<>();
    public static Vector<Vector<String>> colAbstInfos = new Vector<>();
    public static Vector<Vector<String>> oriSyncColNames = new Vector<>();
    public static String projectName = new String();
    public static String uniTableName = new String();
    public static String timestamp = new String();
    public static String method = new String();
    public static String dp1TableName = new String();
    public static String dp2TableName = new String();
    public static Vector<String> dpTableCol1 = new Vector<>();
    public static Vector<String> dpTableCol2 = new Vector<>();
    public static Vector<String> dpTablePKCol1 = new Vector<>();
    public static Vector<String> dpTablePKCol2 = new Vector<>();
    public static String dpTableSJCol1 = new String();
    public static String dpTableSJCol2 = new String();
    public static Vector<Vector<String>> syncColsinfo = new Vector<Vector<String>>();
    public static String syncCase = new String();
//    public static String bbpTableName = new String();

    public static Vector<Vector<String>> mappingInfos = new Vector<>(); // oriCol, oriColRange, rsCol, [rsDistCols]

    public Variable() {
    }

    public Variable(String filePath) throws IOException, ParseException {
        File file = new File(filePath);
        BufferedReader br = null;
        br = new BufferedReader(new FileReader(file));
        String jsonString = new String();
        String s = new String();
        while ((s = br.readLine()) != null) {
            jsonString += s;
        }
        setVariables(jsonString);
    }

    public void setVariables(String jsonString) throws ParseException {
        /*
        json 파서를 생성하고
        json 데이터를 넣어 json 오브젝트를 만든다.
        */
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);

        /*
        url의 배열을 추출
        계속 key 값의 배열을 추출한다.
        */
        url = jsonObject.get("url").toString();
        username = jsonObject.get("username").toString();
        password = jsonObject.get("password").toString();
        method = jsonObject.get("method").toString();
        oriTable = jsonObject.get("oriTable").toString();
//        bbpTableName = jsonObject.get("bbpTableName").toString();
        tfTableName = jsonObject.get("tfTableName").toString();
        groupSize = Integer.parseInt(jsonObject.get("groupSize").toString());
        roundNum = Integer.parseInt(jsonObject.get("roundNum").toString());
        dsUpperBound = Double.parseDouble(jsonObject.get("dsUpperBound").toString());
        dsLowerBound = Double.parseDouble(jsonObject.get("dsLowerBound").toString());
        revisionRange = Double.parseDouble(jsonObject.get("revisionRange").toString());
        projectName = jsonObject.get("projectName").toString();
//        dp1TableName = jsonObject.get("dp1TableName").toString();
//        uniTableName = jsonObject.get("uniTableName").toString();
//        dp2TableName = jsonObject.get("dp2TableName").toString();
//        dpTableSJCol1 = jsonObject.get("dpTableSJCol1").toString();
//        dpTableSJCol2 = jsonObject.get("dpTableSJCol2").toString();
//        syncCase = jsonObject.get("syncCase").toString();

        JSONArray jsonArray = (JSONArray) jsonObject.get("colTransformInfos");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject temp = (JSONObject) jsonArray.get(i);
            Vector<String> tuple = new Vector<>();
            tuple.add(temp.get("colName").toString());
            tuple.add(temp.get("expression").toString());
            tuple.add(temp.get("newColName").toString());
            colTransformInfos.add(tuple);
        }

        jsonArray = (JSONArray) jsonObject.get("oriPKColNames");
        for (int i = 0; i < jsonArray.size(); i++) {
            oriPKColNames.add(jsonArray.get(i).toString());
        }

        jsonArray = (JSONArray) jsonObject.get("oriAKColNames");
        for (int i = 0; i < jsonArray.size(); i++) {
            oriAKColNames.add(jsonArray.get(i).toString());
        }

        jsonArray = (JSONArray) jsonObject.get("oriAKColDomainRanges");
        for (int i = 0; i < jsonArray.size(); i++) {
            oriAKColDomainRanges.add(Double.parseDouble(jsonArray.get(i).toString()));
        }

        jsonArray = (JSONArray) jsonObject.get("oriAbstOptColNames");
        for (int i = 0; i < jsonArray.size(); i++) {
            oriAbstOptColNames.add(jsonArray.get(i).toString());
        }

        // random ordering
        for (int i = 0; i < oriAbstOptColNames.size(); i++) {
            oriAbstOptColNamesOrder.add(i);
        }
        Collections.shuffle(oriAbstOptColNamesOrder);

        jsonArray = (JSONArray) jsonObject.get("oriAbstColNames");
        for (int i = 0; i < jsonArray.size(); i++) {
            oriAbstColNames.add(jsonArray.get(i).toString());
        }

        jsonArray = (JSONArray) jsonObject.get("oriSyncColNames");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONArray temp = (JSONArray) jsonArray.get(i);
            Vector<String> tuple = new Vector<>();
            for (int j = 0; j < temp.size(); j++) {
                tuple.add(temp.get(j).toString());
            }
            oriSyncColNames.add(tuple);
        }

        jsonArray = (JSONArray) jsonObject.get("colAbstInfos");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONArray temp = (JSONArray) jsonArray.get(i);
            Vector<String> tuple = new Vector<>();
            for (int j = 0; j < temp.size(); j++) {
                tuple.add(temp.get(j).toString());
            }
            colAbstInfos.add(tuple);
        }

//        jsonArray = (JSONArray) jsonObject.get("dpTablePKCol1");
//        for(int i=0; i<jsonArray.size(); i++){
//            dpTablePKCol1.add(jsonArray.get(i).toString());
//        }
//
//        jsonArray = (JSONArray) jsonObject.get("dpTablePKCol2");
//        for(int i=0; i<jsonArray.size(); i++){
//            dpTablePKCol2.add(jsonArray.get(i).toString());
//        }
//
//        jsonArray = (JSONArray) jsonObject.get("syncColsInfo");
//        for(int i=0; i<jsonArray.size(); i++){
//            JSONArray temp = (JSONArray)jsonArray.get(i);
//            Vector<String> tuple = new Vector<>();
//            for(int j=0; j<temp.size(); j++){
//                tuple.add(temp.get(j).toString());
//            }
//            syncColsinfo.add(tuple);
//        }
        timestamp = Long.toString(System.currentTimeMillis());
    }

    public void shuffleAbstOptColNames() {
        Collections.shuffle(oriAbstOptColNamesOrder);
    }

    public void changeTimestamp() {
        timestamp = Long.toString(System.currentTimeMillis());
    }

    public void printAll() {
        String log = "=======================================\n";
        log += "url: " + url + "\n";
        log += "username: " + username + "\n";
        log += "password: " + password + "\n";
        log += "\n";
        log += "projectName: " + projectName + "\n";
        log += "oriTable: " + oriTable + "\n";
        log += "tfTableName: " + tfTableName + "\n";
        log += "groupSize: " + groupSize + "\n";
        log += "dsUpperBound: " + dsUpperBound + "\n";
        log += "dsLowerBound: " + dsLowerBound + "\n";
        log += "revisionRange: " + revisionRange + "\n";
        log += "colTransformInfos: " + colTransformInfos + "\n";
        log += "oriPKColNames: " + oriPKColNames + "\n";
        log += "oriAKColNames: " + oriAKColNames + "\n";
        log += "oriAbstOptColNames: " + oriAbstOptColNames + "\n";
        log += "oriAbstOptColNamesOrder: " + oriAbstOptColNamesOrder + "\n";
        log += "oriAbstColNames: " + oriAbstColNames + "\n";
        log += "colAbstInfos: " + colAbstInfos + "\n";
        log += "oriSyncColNames: " + oriSyncColNames + "\n";
        log += "=======================================" + "\n";
        System.out.println(log);
        logger.info(log);
    }

    public void printMappingInfos() {
        System.out.println("0000000000000000000000000000000000000000000");
        for (int i = 0; i < mappingInfos.size(); i++) {
            System.out.println("=======================================");
            System.out.println(mappingInfos.get(i).get(0));
            System.out.println(mappingInfos.get(i).get(1));
            System.out.println(mappingInfos.get(i).get(2));
        }
        System.out.println("0000000000000000000000000000000000000000000");
    }


}
