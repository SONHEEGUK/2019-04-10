import socket
import csv
import pickle
from time import sleep

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('localhost',9999))
s.listen(1)
conn,addr = s.accept()

while 1:
	f = open('/home/heeguk/emp1.csv')

	content = f.read()
	content = content.split('\n')

	schema = content[0]
	msg = ''
	count = 0
	for i in range(1,len(content)):
		msg += content[i] + '\n'
		if count == 1000:
			conn.sendall(msg.encode())
			msg = ''
			count = 0
			sleep(2)
		count += 1

	if msg:
		conn.sendall(msg.encode())
		sleep(2)

	f.close()

conn.close()
