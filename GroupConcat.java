package project;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;


class GroupConcat extends UserDefinedAggregateFunction {

    private StructType _inputDataType;
    private StructType _bufferSchema;
    private DataType _returnDataType;


    public GroupConcat() {

        List<StructField> inputFields = new ArrayList<>();
        inputFields.add(DataTypes.createStructField("x", DataTypes.StringType, true)); // 필드이름, 필드타입, NULL 가능여부 2019-03-25 희국
        _inputDataType = DataTypes.createStructType(inputFields);

        List<StructField> bufferFields = new ArrayList<>();
        bufferFields.add(DataTypes.createStructField("buff", DataTypes.StringType, true));
        _bufferSchema = DataTypes.createStructType(bufferFields);

        _returnDataType = DataTypes.StringType;

    }

    @Override
    public StructType inputSchema() {

        return _inputDataType;

    }

    @Override
    public StructType bufferSchema() {

        return _bufferSchema;

    }

    @Override
    public DataType dataType() {

        return _returnDataType;

    }

    @Override
    public boolean deterministic() {

        return true;

    }

    @Override
    public void initialize(MutableAggregationBuffer buffer) {

        buffer.update(0, null);

    }

    @Override
    public void update(MutableAggregationBuffer buffer, Row input) {

        if (!input.isNullAt(0)) {

            if (buffer.isNullAt(0)) {

                buffer.update(0, input.getString(0));

            } else {
                String newValue = input.getString(0) + buffer.getString(0);
                buffer.update(0, newValue);

            }

        }

    }

    @Override
    public void merge(MutableAggregationBuffer buffer1, Row buffer2) {

        if (!buffer2.isNullAt(0)) {

            if (buffer1.isNullAt(0)) {
                buffer1.update(0, buffer2.getString(0));

            } else {

                String newValue = buffer2.getString(0) + '/' + buffer1.getString(0);
                buffer1.update(0, newValue);

            }

        }

    }

    @Override
    public Object evaluate(Row buffer) {

        if (buffer.isNullAt(0)) {

            return null;

        } else {

            return buffer.getString(0);

        }

    }

}

