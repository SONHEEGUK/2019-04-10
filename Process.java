package project;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.RelationalGroupedDataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.zookeeper.Op;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.functions.*;
import java.sql.Timestamp;

import javax.xml.crypto.Data;
import java.util.Vector;

import java.util.Arrays;

public class Process {
    public static Logger logger = Logger.getLogger(Process.class.getName());

    public static void lowLevelAutoProcess(SparkSession ss, Variable var) throws StreamingQueryException {

        String oriTable = var.oriTable;
        for (int i=0; i<var.mappingInfos.size(); i++) {
        	System.out.println(var.mappingInfos.get(i));
        }
        Vector<Dataset<Row>> virTableList = new Vector<>();
        int i = 0;
        //while (i <= 8 && !var.oriAKColNames.isEmpty()) {
        
        	/*
        	배치 시스템에서 인풋을 받아옴.
        	Dataset<Row> ori = ss.read()
                .format("csv")
                .option("header", "true")
                .option("inferSchema", "true")
                .load("/home/heeguk/emp1.csv");
            
           
            */
        	/*
        	.csv 파일을 스트리밍으로 받아옴.
            StructType MySchema = DataTypes.createStructType(new StructField[] {
    				DataTypes.createStructField("YEAR", DataTypes.IntegerType, true),
    				DataTypes.createStructField("SNUM", DataTypes.IntegerType, true),
    				DataTypes.createStructField("SEXNUM", DataTypes.IntegerType, true),
    				DataTypes.createStructField("AGECODE", DataTypes.IntegerType, true),
    				DataTypes.createStructField("CITYCODE", DataTypes.IntegerType, true),
    				DataTypes.createStructField("HEIGHT", DataTypes.IntegerType, true),
    				DataTypes.createStructField("WEIGHT", DataTypes.IntegerType, true),
    				DataTypes.createStructField("WAIST", DataTypes.IntegerType, true),
    				DataTypes.createStructField("EYELEFT", DataTypes.DoubleType, true),
    				DataTypes.createStructField("EYERIGHT", DataTypes.DoubleType, true),
    				DataTypes.createStructField("SBP", DataTypes.IntegerType, true),
    				DataTypes.createStructField("DBP", DataTypes.IntegerType, true),
    				DataTypes.createStructField("BLOODSUGAR", DataTypes.IntegerType, true),
    				DataTypes.createStructField("CIGAR", DataTypes.IntegerType, true),
    				DataTypes.createStructField("DRINK", DataTypes.IntegerType, true),
    	            DataTypes.createStructField("SEX",  DataTypes.StringType, true)
    	    });
            
            Dataset<Row> ori = ss
  				  .readStream()
  				  .format("csv")
  				  .option("header", "true")
  				  .schema(MySchema)
  				  .csv("/home/heeguk/project").withColumn("timestamp", functions.current_timestamp());
			
            */
            
            // 포트를 통해 데이터를 받아옴
    		Dataset<Row> ori = ss
    				.readStream()
    				.format("socket")
    				.option("host","localhost")
    				.option("port",9999)
    				.option("includeTimestamp", true)
    				.load()
    				.selectExpr("cast(split(value, ',')[0] as integer) as YEAR","cast(split(value, ',')[1] as integer) as SNUM","cast(split(value, ',')[2] as integer) as SEXNUM"
    						,"cast(split(value,',')[3] as integer) as AGECODE","cast(split(value,',')[4] as integer) as CITYCODE","cast(split(value,',')[5] as integer) as HEIGHT"
    						,"cast(split(value,',')[6] as integer) as WEIGHT","cast(split(value,',')[7] as integer) as WAIST","cast(split(value,',')[8] as double) as EYELEFT"
    						,"cast(split(value,',')[9] as double) as EYERIGHT","cast(split(value,',')[10] as integer) as SBP","cast(split(value,',')[11] as integer) as DBP"
    						,"cast(split(value,',')[12] as integer)as BLOODSUGAR","cast(split(value,',')[13] as integer)as CIGAR","cast(split(value,',')[14] as integer) as DRINK"
    						,"cast(split(value,',')[15] as string) as SEX", "cast(split(value,',')[16] as timestamp) as timestamp")
    				.withColumn("timestamp", functions.current_timestamp());
    		
            
            ori.printSchema();
            


            
            // STEP 1. Transform
            System.out.println("Step 1 Start");
            Dataset<Row> cd = Operation.addTFCols(ss, var, ori);
           
            // STEP 2. Make Big Group Index
            System.out.println("Step 2 Start");
            Dataset<Row> cd2 = Operation.largeGrouping(ss, var, cd);
            cd2.printSchema();
            
            System.out.println("step 2-1 finish");
            Dataset<Row> cd3 = Operation.updateFlag(ss, var, cd2, "ambiguous");
            cd3.printSchema();
            
            /*
            	Step 2까지 마치고 write stream을 하려고하면 에러 1번이 뜸.
            	Exception in thread "main" org.apache.spark.sql.AnalysisException: Non-time-based windows are not supported on streaming DataFrames/Datasets;
            StreamingQuery query = cd3.writeStream()
  				  .format("csv")
  				  .option("path","/home/heeguk/finalcsv")
  				  .option("checkpointLocation", "/home/heeguk/finalcsv/checkpoint")
  				  .start();
            
            
            query.awaitTermination();
            */
            
 
            // STEP 3. Make GID (micro group index)
            System.out.println("Step 3 Start");
            Dataset<Row> cd4 = Operation.updateGID(ss, var, cd3);
            System.out.println("Step 3-1 finish");
            Dataset<Row> cd5 = Operation.updateFlag(ss, var, cd4, "remnant");

            
            // STEP 4. Aggregation based on GID
            System.out.println("Step 4 Start");
            Dataset<Row> mp = Operation.microAggregation(ss, var, cd5);
            mp.printSchema();
            
          
            // STEP 5. Calculate Dissimilarity
            System.out.println("Step 5 Start");
            Dataset<Row> ds = Operation.calculateDissimilarity(ss, var, cd5, mp);
            ds.printSchema();
            
            // STEP 6. Revise Dissimilarity
            System.out.println("Step 6 Start, 6 schema");
            Dataset<Row> bds = Operation.reviseDissimilarity(ss, var, ds);
            bds.printSchema();
            
            System.out.println("Step 7 Start, 7 schema");
            // STEP 7. Make Final Result (aggregation revised table)
            // Not bds! 
            Dataset<Row> rs = Operation.makeResultTable(ss, var, ds, cd4);

            rs.printSchema();
            
            /*
            	결과 테이블을 write stream 할 때 에러 2번이 발생됨.
            	Exception in thread "main" org.apache.spark.sql.AnalysisException: Multiple streaming aggregations are not supported with streaming DataFrames/Datasets;
            StreamingQuery query = rs.writeStream()
    				  .format("csv")
    				  .option("path","/home/heeguk/finalcsv")
    				  .option("checkpointLocation", "/home/heeguk/finalcsv/checkpoint")
    				  .start();
              
              
            query.awaitTermination();
            
            */
            
			/*
  			해당 부분은 진행하지 않음.	  
            virTableList.add(rs);
            if (i >= 3) {
                if (rs.count() < 1) break;
            }
            System.out.println("Last step Start");
            
            Dataset<Row> rs2 = Operation.makeAbsenteeTable_Low(ss, var, rs, ori);
            
            System.out.println("Last step End");
            
            rs2.write()
            .format("csv")
            .option("header", "true")
            .save("/home/heeguk/step8");
			
            oriTable = var.projectName + "_ABS_" + i;
            String tempCol = var.oriAKColNames.lastElement();
            Double tempColRange = var.oriAKColDomainRanges.lastElement();
            String tempColRang = tempColRange.toString();

            var.oriAKColNames.removeElement(tempCol);
            var.oriAKColDomainRanges.removeElement(tempColRange);

            Vector<String> colAbstInfosRe = new Vector<>();
            var.oriAbstColNames.add(tempCol);
            colAbstInfosRe.add(tempCol);
            colAbstInfosRe.add("numeric");
            colAbstInfosRe.add("concat");
            colAbstInfosRe.add("");
            colAbstInfosRe.add(tempColRang);
            var.colAbstInfos.add(colAbstInfosRe);
            var.oriAbstOptColNames.add(tempCol);
            var.oriAbstColNames.add(tempCol);
            var.mappingInfos = new Vector<Vector<String>>();
            i++;
            */
            //query.awaitTermination();
        //}

        /*
        Dataset<Row> unionres = Operation.unionAllResultTables(ss, var, virTableList);
        unionres.write()
        .format("csv")
        .option("header", "true")
        .save("/home/heeguk/final.csv");
        */
        
    }

 
    

}
