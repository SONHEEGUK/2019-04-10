package project;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    protected static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws Exception {
    	
        PatternLayout patternLayout = new PatternLayout();
        patternLayout.setConversionPattern("%d %-5p [%t] %C{2} (%F:%L) - %m%n");

        FileAppender appender = new FileAppender(patternLayout, new SimpleDateFormat("[yyyy-MM-dd]HH.mm.ss").format(new Date()) + "_error.log", false);
        Operation.logger.addAppender(appender);

        /*
         * Set variables (params)
         * Method 1. json string -- setVariables("{ params: ... }");
         * Method 2. json file -- new Variable("your/json/file.json");
        JSON Object params = new JSONObject();
         */
        
        /*
        Variable.java 占쏙옙占쏙옙
        */
        Variable var = new Variable("/home/heeguk/params.json");
        var.printAll();
        
        SparkSession ss = SparkSession
                .builder()
                .config("spark.sql.broadcastTimeout", 20000)
                .appName("MAS-STREAM")
                .getOrCreate();
        /*
        spark.udf.register("占쏙옙占쏙옙占쏙옙占쏙옙 占쌉쇽옙占쏙옙", 占쏙옙占쌜뤄옙占싱쇽옙)
        Spark占쏙옙占쏙옙 sql占쏙옙占쏙옙 占쏙옙, 占쏙옙占쏙옙微占� 占쏙옙占쏙옙 占쏙옙占쏙옙 占쌉쇽옙占쏙옙 占쏙옙 占쏙옙 占쌍곤옙 占쏙옙占쏙옙
        2019-03-25 占쏙옙
        */
        ss.udf().register("rand", UDF.rand, DataTypes.FloatType);
        ss.udf().register("group_concat", new GroupConcat());
        ss.udf().register("split_length", UDF.splitLength, DataTypes.IntegerType);
        ss.udf().register("split_distinct_length", UDF.splitDistinctLength, DataTypes.IntegerType);

        /*
        process.java 占쏙옙占쏙옙占싹깍옙
        */
        if (var.method.equals("LA")) {
            Process.lowLevelAutoProcess(ss, var);
        }else {
            logger.error("There is no method that name is " + var.method + " \n Please try LA or HA of LS or HS");
        };

        ss.stop();
    }
}
