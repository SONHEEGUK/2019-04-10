package project;

import org.apache.spark.sql.api.java.UDF1;

import java.util.HashSet;
import java.util.Random;

/**
 * Created by O on 2017-07-16.
 */
public class UDF {
    public static UDF1 rand = new UDF1<Integer, Float>() {
        @Override
        public Float call(Integer seed) throws Exception {
            Random rand = new Random();
            if(seed != 0) rand.setSeed(seed);
            return rand.nextFloat();
        }
    };

    public static UDF1 splitLength = new UDF1<String, Integer>() {
        @Override
        public Integer call(String s) throws Exception {
            return s.split("/").length;
        }
    };

    public static UDF1 splitDistinctLength = new UDF1<String, Integer>() {
        @Override
        public Integer call(String s) throws Exception {
            HashSet<String> hashSet = new HashSet<>();
            String[] ss = s.split("/");
            for(int i=0; i<ss.length; i++){
                hashSet.add(ss[i]);
            }
            return hashSet.size();
        }
    };
}
