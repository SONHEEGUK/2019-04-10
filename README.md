## 기존 코드에서 변경 사항

- 데이터베이스 자료를 그대로 가져올 때 컬럼 명이 한글이라 깨지는 문제가 생겨서
영어로 변경. 그 외 필요없는 컬럼들을 삭제함. (모두 삭제하진 못함)

- 데이터베이스의 컬럼명이 변경되어서 json 파일또한 변경 됨.

- Operation.java 파일이 변경됨.
timestamp 컬럼이 추가되어서 각 쿼리마다 timestamp를 가져오는 쿼리가 추가됨.
timestamp를 추가한 뒤 배치시스템 으로는 잘 작동 됨.


## READSTREAM 구현

- 스트리밍을 구현할 때 소켓을 통해 구현

- input.py에서 정해진 디렉토리에 있는 csv 파일을 가져와서 줄 단위로
스파크 어플리케이션에 보냄

- Process.java에서 소켓으로 데이터를 받을때 부가적으로 timestamp라는 컬럼을 추가한다.

- 그 외 csv 파일을 스트리밍으로 받아오는 방법을 구현하였고 주석처리 하였음.

## WRITESTREAM 구현시 오류

- 에러 1
    - Exception in thread "main" org.apache.spark.sql.AnalysisException: Non-time-based windows are not supported on streaming DataFrames/Datasets;
    - Step 1 과정이 끝난 뒤 대 그룹화된 데이터를 출력할 때 해당 에러가 나옴.
    - 에러가 생기는 부분은 Process.java에 주석처리 해놓았음.

- 에러 2
    - Exception in thread "main" org.apache.spark.sql.AnalysisException: Multiple streaming aggregations are not supported with streaming DataFrames/Datasets;
    - Step 7 과정이 끝난 뒤 결과 테이블을 출력할 때 해당 에러가 나옴.
    - 에러가 생기는 부분은 Process.java에 주석처리 해놓았음.

