package project;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;
import org.apache.zookeeper.Op;
import sun.awt.image.ImageWatched;

import javax.xml.crypto.Data;
import java.util.*;

public class Operation {

    public static Logger logger = Logger.getLogger(Operation.class.getName());
    /*
    */
    public static Boolean getQuery = true;

    public static Dataset<Row> addTFCols(SparkSession ss, Variable var, Dataset<Row> ds) {
        /*
         * Add transformation columns
         * query -- select A.*, <expression> as <New column name> from A
         */

        if (var.colTransformInfos.size() > 0) {
            return ss.sql(addTFCols(ss, var, ds, getQuery));
        } else {
            return ds;
        }
    }

    public static String addTFCols(SparkSession ss, Variable var, Dataset<Row> ds, Boolean getQuery) {
        /*
        */
        ds.createOrReplaceTempView("A");
        String query = "";
        query += "select A.* ";
        for (int i = 0; i < var.colTransformInfos.size(); i++) {
            query += ", " + var.colTransformInfos.get(i).get(1) + " as " + var.colTransformInfos.get(i).get(2);
        }
        query += " from A ";
        logger.info("[Add Transformation Columns] " + query + " " + var.timestamp);
        return query;
    }

    public static Dataset<Row> largeGrouping(SparkSession ss, Variable var, Dataset<Row> ds) {
        /*
         * Make large group by gid_index
         * gid_index made by concatenating abstraction key columns
         * in this step, gid_index only represents large group
         * query --
         */

        return ss.sql(largeGrouping(ss, var, ds, getQuery));
    }

    public static String largeGrouping(SparkSession ss, Variable var, Dataset<Row> ds, Boolean getQuery) {
        /*
        */
        ds.createOrReplaceTempView("A");
        String query = "select B.*, (row_number() over (partition by GID_INDEX order by ";
        int pksize = var.oriPKColNames.size();
        pksize = pksize - 1;

        String sortingOrder = new String();

        logger.info(var.oriAbstOptColNames);
        logger.info(var.oriAbstOptColNamesOrder);
        if (var.oriAbstOptColNames.size() > 0) {
            for (int i = 0; i < var.oriAbstOptColNamesOrder.size(); i++) {
                if (i != 0) sortingOrder += ", ";
                sortingOrder += var.oriAbstOptColNames.get(var.oriAbstOptColNamesOrder.get(i));
            }
        }
        // sortingOrder : col1, col6, col7, ... , colN
        query += sortingOrder;
        query += " ) - 1) as BG_INDEX";

        for (int idx = 0; idx < var.oriSyncColNames.size(); idx++) {
            query += ", concat(";
            Vector<String> hashColInfo = var.oriSyncColNames.get(idx);
            String colName = hashColInfo.get(0);
            String bucket = hashColInfo.get(1);
            String seed = hashColInfo.get(2);
            int bucketCiphers = bucket.length();
            if (idx != 0) {
                query += ", ";
            }
            query += "md5(concat(" + colName + ", " + seed + "))";
        }
        if (var.oriSyncColNames.size() > 0)
        	query += " ) as rec_dist ";

        query += " from (select ori.*, concat( ";

        for (int i = 0; i < var.oriPKColNames.size(); i++) {
            query += var.oriPKColNames.get(i);
            if (i < pksize) query += ", ";
        }
        query += " ) as COMPLETEPK , ";
        query += "cast( md5( concat('" + var.timestamp + "' ";
        for (int i = 0; i < var.oriAKColNames.size(); i++) {
            query += ", " + var.oriAKColNames.get(i) + " ";
        }
        query += ")) as varchar(60)) as GID_INDEX ";
        query += "from A ori) B";

        logger.info("[Add Big Group Index] " + query + " " + var.timestamp);
        return query;
    }

    public static Dataset<Row> updateFlag(SparkSession ss, Variable var, Dataset<Row> ds, String flag) {
        /*
         * Update flag e.g. ambiguous, remnant
         * query --
         */

        return ss.sql(updateFlag(ss, var, ds, flag, getQuery));
    }

    public static String updateFlag(SparkSession ss, Variable var, Dataset<Row> ds, String flag, Boolean getQuery) {
        //todo performance issue about 'union' instead of 'update query'

        ds.createOrReplaceTempView("A2");
        String[] schema = ds.schema().fieldNames();
        String selectColNames = "";
        for (String s : schema) {
            if (!(s.equals("FLAG"))) selectColNames += s + ", ";
        }
        String dissatisfaction = " select 1 from A2 T where T.GID_INDEX = A2.GID_INDEX group by GID_INDEX having count(GID_INDEX) = " + var.groupSize;
        String query = "select " + selectColNames
                + " cast(null as varchar(15)) as FLAG from A2 where not exists ("
                + dissatisfaction + " )"
                + " union "
                + "select " + selectColNames
                + " '" + flag + "' as FLAG from A2 where exists ("
                + dissatisfaction + " ) ";

        logger.info("[Append Flag] " + query);
        return query;
    }

    public static Dataset<Row> updateGID(SparkSession ss, Variable var, Dataset<Row> ds) {
        /*
         * Update gid_index into gid_index + micro_index
         * in this step, gid_index represents large group and micro group
         * query --
         */

        return ss.sql(updateGID(ss, var, ds, getQuery));
    }

    public static String updateGID(SparkSession ss, Variable var, Dataset<Row> ds, Boolean getQuery) {
        ds.createOrReplaceTempView("A");
        String[] schema = ds.schema().fieldNames();
        String query = "select ";
        for (String s : schema) {
            if (!s.equals("GID_INDEX")) query += s + " , ";
        }
        query += " cast(concat( GID_INDEX, '_', round( (BG_index/" + var.groupSize + " )) ) as varchar(60)) as GID_INDEX "
                + "from A";

        logger.info("[Add GID] " + query);
        return query;
    }

    public static Dataset<Row> microAggregation(SparkSession ss, Variable var, Dataset<Row> ds) {
        /*
         * Aggregation N tuples -> 1 tuples using (user-inputed) grouping methods
          * and distribution columns
         * query -- select max(AGE_GRP) as AGE_GRP,avg(INCOME) as INCOME_avg,stddev(INCOME) as INCOME_stddev, gid_index
                    from dete_CD_BANK
                    group by gid_index having count(gid_index) = 2;
         */
    	return ss.sql(microAggregation(ss, var, ds, getQuery));
    }

    public static String microAggregation(SparkSession ss, Variable var, Dataset<Row> ds, Boolean getQuery) {
        ds.createOrReplaceTempView("A");
        String query = "select MAX(timestamp) as timestamp,";

        for (int i = 0; i < var.oriAKColNames.size(); i++) {
            if (!(var.oriAKColNames.get(i).equals("K13"))) {
                query += " max(" + var.oriAKColNames.get(i).trim() + ") as " + var.oriAKColNames.get(i).trim() + ", ";
                query += " max(" + var.oriAKColNames.get(i).trim() + ") as G_" + var.oriAKColNames.get(i).trim() + ", ";
                Vector<String> mappingInfo = new Vector<>();
                mappingInfo.add(var.oriAKColNames.get(i));
                mappingInfo.add(Double.toString(var.oriAKColDomainRanges.get(i)));
                mappingInfo.add("G_" + var.oriAKColNames.get(i));
                var.mappingInfos.add(mappingInfo);
            }
        }
        
        for (int i = 0; i < var.colAbstInfos.size(); i++) {
            Vector<String> mappingInfo = new Vector<>();

            String oriColName = var.colAbstInfos.get(i).get(0).trim();
            String oriColType = var.colAbstInfos.get(i).get(1).trim();
            String groupingMethod = var.colAbstInfos.get(i).get(2).trim();
            String[] groupingDistMethods = var.colAbstInfos.get(i).get(3).trim().split(",");
            double domainRange = Double.parseDouble(var.colAbstInfos.get(i).get(4).trim());

            mappingInfo.add(oriColName);
            mappingInfo.add(Double.toString(domainRange));

            // �׷��� method
            switch (groupingMethod) {
                case "average":
                    query += "avg(" + oriColName + ") as G_" + oriColName + "_AVG , ";
                    mappingInfo.add("G_" + oriColName + "_AVG");
                    break;
                case "minimum":
                    query += "min(" + oriColName + ")  as G_" + oriColName + "_MIN , ";
                    mappingInfo.add("G_" + oriColName + "_MIN");
                    break;
                case "maximum":
                    query += "max(" + oriColName + ")  as G_" + oriColName + "_MAX , ";
                    mappingInfo.add("G_" + oriColName + "_MAX");
                    break;
                case "frequent":
                    //todo grouping using frequent
                    break;
                case "concat":
                    query += "group_concat(" + oriColName + ")  as G_" + oriColName + "_CNCT , ";
                    mappingInfo.add("G_" + oriColName + "_CNCT");
                    break;
                case "random":
                    //todo grouping using random
                default:
                    break;

            }

            
            // ���� �Ӽ� �߰�
            for (int j = 0; j < groupingDistMethods.length; j++) {
                switch (groupingDistMethods[j]) {
                    case "standard_deviation":
                        query += "stddev(" + oriColName + ") as G_" + oriColName + "_DSTD , ";
                        mappingInfo.add("G_" + oriColName + "_DSTD");
                        break;
                    case "minimum":
                        query += "min(" + oriColName + ")  as G_" + oriColName + "_DMN , ";
                        mappingInfo.add("G_" + oriColName + "_DMN");
                        break;
                    case "max":
                        query += "max(" + oriColName + ")  as G_" + oriColName + "_DMX , ";
                        mappingInfo.add("G_" + oriColName + "_DMX");
                        break;
                    case "count":
                        query += "count(distinct " + oriColName + ")  as G_" + oriColName + "_DCNT , ";
                        mappingInfo.add("G_" + oriColName + "_DCNT");
                        break;
                    default:
                        break;
                }
            }
            var.mappingInfos.add(mappingInfo);
        }
        query += " GID_INDEX from A group by GID_INDEX having count(GID_INDEX) =" + var.groupSize;
        logger.info("[Aggregation based on GID_INDEX] " + query + " " + var.timestamp);
        return query;
    }

    public static Dataset<Row> calculateDissimilarity(SparkSession ss, Variable var, Dataset<Row> cd, Dataset<Row> mp) {
        /*
         * Calculate dissimilarity btw candidate table and mapped(aggregated N to 1) table
         * query --  select A.gid_index, A.completePK , A.ID as key_ID,
         *            A.CREDIT_GRADE, B.CREDIT_GRADE as GCREDIT_GRADE,
         *            A.ZIP_PREFIX, B.ZIP_PREFIX as GZIP_PREFIX,
         *            A.AGE_GRP, B.AGE_GRP as GAGE_GRP,
         *            A.INCOME, B.INCOME_avg as GINCOME_avg, B.INCOME_stddev as GINCOME_stddev,
         *            A.AGE, B.AGE_avg as GAGE_avg, B.AGE_stddev as GAGE_stddev,
         *            abs(A.INCOME - B.INCOME_avg ) as INCOME_d , NVL(abs(A.INCOME - B.INCOME_avg ) / DECODE(24100.0,0,null,24100.0),0) as INCOME_r ,
         *            abs(A.AGE - B.AGE_avg ) as AGE_d , NVL(abs(A.AGE - B.AGE_avg ) / DECODE(0.0,0,null,0.0),0) as AGE_r ,
         *            sqrt(power(NVL(NVL(abs(A.INCOME - B.INCOME_avg ) / DECODE(24100.0,0,null,24100.0),0),0),2) +power(NVL(NVL(abs(A.AGE - B.AGE_avg ) / DECODE(0.0,0,null,0.0),0),0),2) +0)/sqrt(2) as rec_disim
         *            from dete_CD_BANK A, dete_MP_BANK B
         *            where A.gid_index = B.gid_index
         */
        return ss.sql(calculateDissimilarity(ss, var, cd, mp, getQuery));
    }

    public static String calculateDissimilarity(SparkSession ss, Variable var, Dataset<Row> cd, Dataset<Row> mp, Boolean getQuery) {
        cd.createOrReplaceTempView("cd");
        mp.createOrReplaceTempView("mp");
        String query = "select cd.timestamp, cd.GID_INDEX, cd.COMPLETEPK, ";
        if (var.oriPKColNames.size() > 0) {
            for (int i = 0; i < var.oriPKColNames.size(); i++) {
                query += " cd." + var.oriPKColNames.get(i) + " as KEY_" + var.oriPKColNames.get(i) + " , ";
            }
        }

        System.out.println("B");
        for (int i = 0; i < var.mappingInfos.size(); i++) {
            Vector<String> mappingInfo = var.mappingInfos.get(i);
            String oriColName = mappingInfo.get(0);
            query += " cd." + oriColName + ", ";
            for (int j = 2; j < mappingInfo.size(); j++) {
                query += " mp." + mappingInfo.get(j) + " as " + mappingInfo.get(j) + ", ";
            }
        }
        System.out.println(query);
        System.out.println("A");
        String recDisim = "( ";
        for (int i = 0; i < var.mappingInfos.size(); i++) {
            Vector<String> mappingInfo = var.mappingInfos.get(i);
            String oriColName = mappingInfo.get(0);
            Double domainRange = Double.parseDouble(mappingInfo.get(1));
            String grpColName = mappingInfo.get(2);
            if (grpColName.substring(grpColName.length() - 4, grpColName.length()).equals("CNCT")) {
//              query += " round( ((split_distinct_length( " + grpColName + ") - 1 ) / " + domainRange + " ), " + var.roundNum + " ) as " + oriColName + "_R, ";
                recDisim += " power( round( 0, " + var.roundNum + " ) , 2) + ";
            } else {
                if (grpColName.substring(grpColName.length() - 4, grpColName.length()).equals("_AVG")) {
                    query += " round( abs(cd." + oriColName + " - mp." + grpColName + " )  " + " ) as " + oriColName + "_D, ";
                    recDisim += " power( round( abs(cd." + oriColName + " - mp." + grpColName + " ) / " + domainRange + " , " + var.roundNum + " ) , 2) + ";
                }
            }
        }
        System.out.println(query);
        recDisim += " 0 ) / " + var.mappingInfos.size() + " ";
        query += " round(" + recDisim + " , " + var.roundNum + ")  as REC_DISIM from cd, mp where cd.GID_INDEX = mp.GID_INDEX";

        logger.info("[Calculate Dissimilarity] " + query + " " + var.timestamp);
        
        System.out.println("last query");
        System.out.println(query);
        return query;
    }



    public static Dataset<Row> unionAllResultTables(SparkSession ss, Variable var, Vector<Dataset<Row>> dss) {
        /*
         * Union all dissimilarity tables (in high level process)
         * query --  select * from ds1
         *           union
         *           select * from ds2
         *           ...
         */

        return ss.sql(unionAllResultTables(ss, var, dss, getQuery));
    }

    ;

    public static String unionAllResultTables(SparkSession ss, Variable var, Vector<Dataset<Row>> dss, Boolean getQuery) {

        String query = new String();
        String[] schema = {};
        for (int i = 0; i < dss.size(); i++) {
            dss.get(i).createOrReplaceTempView("rs" + i);
            schema = dss.get(0).schema().fieldNames();
        }

        for (int i = 0; i < dss.size(); i++) {
            if (i != 0) query += " union ";
            query += " select ";
            for (String s : schema) {
                query += "rs" + i + "." + s;
                if (!s.equals("rec_dist")) query += ", ";
            }
            query += " from rs" + i + " ";
        }

        logger.info("[Union All Results] " + query);
        return query;
    }


    public static Dataset<Row> reviseDissimilarity(SparkSession ss, Variable var, Dataset<Row> ds) {
        /*
         * description
         * query --
         */

//        Dataset<Row> newOutbound = ss.sql(reviseDissimilarity(ss, var, ds, getQuery));
//        newOutbound.createOrReplaceTempView("rev");

//        String query = "select * from inbound union select * from rev";
//        Dataset<Row> union = ss.sql(query);
//        logger.info(query);
        return ss.sql(reviseDissimilarity(ss, var, ds, getQuery));
    }

    public static String reviseDissimilarity(SparkSession ss, Variable var, Dataset<Row> ds, Boolean getQuery) {
        ds.createOrReplaceTempView("A5");
//        String query = "select * from A5 ";

        String query = "select * from A5 where REC_DISIM >= " + var.dsUpperBound + " or REC_DISIM <= " + var.dsLowerBound;
        Dataset<Row> outbound = ss.sql(query);
        query = "select * from A5 where REC_DISIM <= " + var.dsUpperBound + " and REC_DISIM >= " + var.dsLowerBound;
        Dataset<Row> inbound = ss.sql(query);

        outbound.createOrReplaceTempView("outbound");
        inbound.createOrReplaceTempView("inbound");

        var.printMappingInfos();

        LinkedList<String> groupColNames = new LinkedList<>();
        for (int i = 0; i < var.mappingInfos.size(); i++) {
            groupColNames.add(var.mappingInfos.get(i).get(2));
        }

        StructType schema = outbound.schema();
        query = "select ";
        for (String fieldName : schema.fieldNames()) {
            if (fieldName.equals("REC_DISIM")) continue;
            if (fieldName.substring(fieldName.length() - 2, fieldName.length()).equals("_R")) continue;
            if (!groupColNames.contains(fieldName)) query += fieldName += " , ";
        }

        for (String groupColName : groupColNames) {
//            query += groupColName + "+ IF( ( ( (REC_DISIM - " + var.dsLowerBound + ") / abs(REC_DISIM - " + var.dsLowerBound
//                    + ") ) + ( (REC_DISIM - " + var.dsUpperBound + ") / abs(REC_DISIM - " + var.dsUpperBound
//                    + ") ) ) = 2, -1, IF( ( ( (REC_DISIM - " + var.dsLowerBound + ") / abs(REC_DISIM - " + var.dsLowerBound
//                    + ") ) + ( (REC_DISIM - " + + var.dsUpperBound + ") / abs(REC_DISIM - " + var.dsUpperBound
//                    + ") ) ) = -2, 1, 0)) * " + var.revisionRange + " * rand(0) as " + groupColName + ",";

            query += groupColName + "+ (rand(0) - 0.5) * " + var.revisionRange + " as " + groupColName + ",";
        }

        query = query.substring(0, query.length() - 1);

        query += " from outbound";
        Dataset<Row> revisedOutbound = ss.sql(query);
        revisedOutbound.createOrReplaceTempView("revised"); //

        query = "select *, ";


        String recDisim = "( ";
        for (int i = 0; i < var.mappingInfos.size(); i++) {
            Vector<String> mappingInfo = var.mappingInfos.get(i);
            String oriColName = mappingInfo.get(0);
            Double domainRange = Double.parseDouble(mappingInfo.get(1));
            String grpColName = mappingInfo.get(2);
            if (!(oriColName.equals("major_code") || oriColName.equals("minor_code"))) {
                recDisim += " power( abs(" + oriColName + " - " + grpColName + " ) / " + domainRange + " , 2) + ";
            }
        }
        recDisim += " 0 ) / " + var.mappingInfos.size() + " ";

        query += " IF( " + recDisim + " < 0.001 , 0.001, " + recDisim + ") as REC_DISIM from revised ";
        logger.info("[Revise Dissimilarity] " + query + " " + var.timestamp);
        return query;
    }

    public static Dataset<Row> makeResultTable(SparkSession ss, Variable var, Dataset<Row> bds, Dataset<Row> ori) {
        /*
         * description
         * query --
         */

        return ss.sql(makeResultTable(ss, var, bds, ori, getQuery));
    }

    public static String makeResultTable(SparkSession ss, Variable var, Dataset<Row> bds, Dataset<Row> ori, Boolean getQuery) {
        bds.createOrReplaceTempView("A");
        ori.createOrReplaceTempView("ori");

        String candiSyncSQL = new String();
        String candiVirSQL = new String();
        String STcolName = new String();
        String query = new String();
        Vector<String> STcol = new Vector<String>();
/*
        StructType schema = ori.schema();
        for (String s : schema.fieldNames()) {
            if (s.substring(s.length() - 2, s.length()).equals("ST")) STcol.add(s);
        }
        ;
        int stsize = STcol.size();
        stsize = stsize - 1;

        String[] schema2 = bds.schema().fieldNames();
        String selectColNames = "";
        for (String s : schema2) {
            if (!(s.equals("KEY_PK"))) selectColNames += "bds." + s + " as " + s + ", ";
        }

        query += "select " + selectColNames;
        for (int i = 0; i < STcol.size(); i++) {
            STcolName = STcol.get(i);
            query += "cd5." + STcolName + " as " + STcolName;
            if (i < stsize) query += ", ";
        }

        query += " cd5.rec_dist from bds, cd5 where bds.completepk = cd5.completepk ";
*/
        candiVirSQL = "select max(timestamp) as timestamp, max(C.GID_INDEX) as ABST_ID, max(C.completepk) as ORI_ID, ";

        for (int i = 0; i < var.oriAKColNames.size(); i++) {
            candiVirSQL += " max(C.G_" + var.oriAKColNames.get(i).trim() + ") as " + var.oriAKColNames.get(i).trim() + ",";
        }

        for (int i = 0; i < var.colAbstInfos.size(); i++) {

            String oriColName = var.colAbstInfos.get(i).get(0).trim();
            String oriColType = var.colAbstInfos.get(i).get(1).trim();
            String groupingMethod = var.colAbstInfos.get(i).get(2).trim();
            String[] groupingDistMethods = var.colAbstInfos.get(i).get(3).trim().split(",");
            double domainRange = Double.parseDouble(var.colAbstInfos.get(i).get(4).trim());

            // �׷��� method
            switch (groupingMethod) {
                case "average":
                    candiVirSQL += "avg(C.G_" + oriColName + "_AVG) as " + oriColName + "_AVG ,";
                    break;
                case "minimum":
                    candiVirSQL += "min(C.G_" + oriColName + "_MIN) as " + oriColName + "_MIN ,";
                    break;
                case "maximum":
                    candiVirSQL += "max(C.G_" + oriColName + "_MAX) as " + oriColName + "_MAX ,";
                    break;
                case "frequent":
                    //todo grouping using frequent
                    break;
                case "concat":
                    candiVirSQL += "first(C.G_" + oriColName + "_CNCT) as " + oriColName + "_CNCT ,";
                    break;
                case "random":
                    //todo grouping using random
                default:
                    break;
            }

            // ���� �Ӽ� �߰�
            for (int j = 0; j < groupingDistMethods.length; j++) {
                switch (groupingDistMethods[j]) {
                    case "standard_deviation":
                        candiVirSQL += "stddev(C.G_" + oriColName + "_DSTD) as " + oriColName + "_DSTD ,";
                        break;
                    case "minimum":
                        candiVirSQL += "min(C.G_" + oriColName + "_DMN) as " + oriColName + "_DMN ,";
                        break;
                    case "maximum":
                        candiVirSQL += "max(C.G_" + oriColName + "_DMX) as " + oriColName + "_DMX ,";
                        break;
                    case "count":
                        candiVirSQL += "count(C.G_" + oriColName + "_DCNT) as " + oriColName + "_DCNT ,";
                        break;
                    default:
                        break;
                }
            }
        }
        candiVirSQL += " max(C.REC_DISIM) as REC_DISIM from A C group by C.GID_INDEX having count(C.GID_INDEX) =  " + var.groupSize;
        Dataset<Row> candiVir = ss.sql(candiVirSQL);
        candiVir.createOrReplaceTempView("candi");

        //��Ű�� 2���� / selectColNames
        String[] schema2 = bds.schema().fieldNames();
        String selectColNames = "";
        for (String s : schema2) {
            if (!(s.substring(0, 3).equals("KEY"))) selectColNames += "K." + s + " as " + s + ", ";
        }

        query += " from A K, ori B where K.GID_INDEX = B.GID_INDEX ";
        System.out.println(candiVirSQL);
        logger.info("[Make Result] " + candiVirSQL + " " + var.timestamp);
        logger.info("[Make Result] " + query + " " + var.timestamp);
        return candiVirSQL;
        //return query;
    }


    public static Dataset<Row> makeAbsenteeTable_Low(SparkSession ss, Variable var, Dataset<Row> cd, Dataset<Row> ori) {
        /*
         * Make absentee table ( All record's flag is not null )
         * query -- select A.*
         * 			from dete_TP_BANK A, dete_CD_BANK B
         * 			where B.FLAG IS NOT NULL AND A.ID = B.COMPLETEPK
         */

        return ss.sql(makeAbsenteeTable_Low(ss, var, cd, ori, getQuery));
    }

    public static String makeAbsenteeTable_Low(SparkSession ss, Variable var, Dataset<Row> cd, Dataset<Row> ori, Boolean getQuery) {
        cd.createOrReplaceTempView("cd");
        ori.createOrReplaceTempView("A");

        String query = "select A.* ";
        query += "from ori A ";
        query += "where not exists (select B.* from cd B where B.ORI_ID = A." + var.oriPKColNames.get(0) + ") ";
        //�̰� ���� query += "where not exists (select B.* from cd B where B.completepk = A." + var.oriPKColNames.get(0) + ") ";
        logger.info("[Make Absentee Table] " + query);
        return query;
    }

    public static Dataset<Row> removeSimilarity(SparkSession ss, Variable var, Dataset<Row> rbp) {

        /*
            Remove Similarity and Re-Calculate REC_DISIM
         */

        return ss.sql(removeSimilarity(ss, var, rbp, getQuery));
    }

    public static String removeSimilarity(SparkSession ss, Variable var, Dataset<Row> rbp, Boolean getQuery) {

        rbp.createOrReplaceTempView("rbp");
        StructType schema = rbp.schema();
        String selectColNames = "";
        for (String s : schema.fieldNames()) {
            if (!(s.equals("REC_DISIM") || s.substring(s.length() - 2, s.length()).equals("_D") || s.substring(s.length() - 2, s.length()).equals("_R")))
                selectColNames += s + ", ";

        }
        ;

        String query = new String();
        Vector<Vector<String>> colRevisionInfo = new Vector<Vector<String>>();

        query = "select * from rbp";
        String recDisim = "( ";
        for (int i = 0; i < var.mappingInfos.size(); i++) {
            Vector<String> mappingInfo = var.mappingInfos.get(i);
            String oriColName = mappingInfo.get(0);
            Double domainRange = Double.parseDouble(mappingInfo.get(1));
            String grpColName = mappingInfo.get(2);
            if (grpColName.substring(grpColName.length() - 4, grpColName.length()).equals("CNCT")) {
                query += " round( ((split_distinct_length( " + grpColName + ") - 1 ) / " + domainRange + " ), " + var.roundNum + " ) as " + oriColName + "_R, ";
                recDisim += " power( round( 0, " + var.roundNum + " ) , 2) + ";
            } else {
                query += " round( abs(" + oriColName + " - " + grpColName + " )  " + " ) as " + oriColName + "_D, ";
                query += " round( abs(" + oriColName + " - " + grpColName + " ) / " + domainRange + " , " + var.roundNum + " ) as " + oriColName + "_R, ";
                recDisim += " power( round( abs(" + oriColName + " - " + grpColName + " ) / " + domainRange + " , " + var.roundNum + " ) , 2) + ";
            }

        }
        recDisim += " 0 ) / " + var.mappingInfos.size() + " ";
        query += " round(" + recDisim + " , " + var.roundNum + ") as REC_DISIM from rbp";

        logger.info("[Make BBP Table] " + query);
        return query;
    }

}
